﻿using ContactManager.Models;
using System;
using System.Linq;
using System.Web;

namespace ContactManager.Services
{
    public class ContactRepository
    {
        private const string CacheKey = "ContactStore";

        public ContactRepository()
        {
            var ctx = HttpContext.Current;

            if (ctx == null) return;
            
            if (ctx.Cache[CacheKey] != null) return;

            var contacts = new[]
            {
                new Contact
                {
                    Id = 1, Name = "Glenn Block"
                },
                new Contact
                {
                    Id = 2, Name = "Dan Roth"
                }
            };

            ctx.Cache[CacheKey] = contacts;
        }

        public Contact[] GetAllContacts()
        {
            var ctx = HttpContext.Current;

            if (ctx != null)
            {
                return (Contact[]) ctx.Cache[CacheKey];
            }

            return new[]
            {
                new Contact
                {
                    Id = 0,
                    Name = "Placeholder"
                }
            };
        }
        public bool SaveContact(Contact contact)
        {
            var ctx = HttpContext.Current;

            if (ctx == null) return false;

            try
            {
                var currentData = ((Contact[])ctx.Cache[CacheKey]).ToList();
                currentData.Add(contact);
                ctx.Cache[CacheKey] = currentData.ToArray();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
    }
}