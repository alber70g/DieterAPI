﻿using System.Net.Http;
using System.Web.Http;
using ContactManager.Models;
using ContactManager.Services;

namespace ContactManager.Controllers
{
public class ContactController : ApiController
{
    private readonly ContactRepository _contactRepository;

    public ContactController()
    {
        _contactRepository = new ContactRepository();
    }

    public Contact[] Get()
    {
        return _contactRepository.GetAllContacts();
    }

    [HttpPost]
    public HttpResponseMessage PostContact([FromBody] Contact contact)
    {
        _contactRepository.SaveContact(contact);

        var response = Request.CreateResponse(System.Net.HttpStatusCode.Created, contact);

        return response;
    }
}}
